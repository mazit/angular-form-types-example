import {ManagementsApiService} from './mot-lazy-api.service';
import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({providedIn: 'root'})
export class NotLazyService {
  // constructor(private api: ManagementsApiService) {
  // }
  //
  // getStoriesData(payload: IGetStoriesRequestModel): Observable<IStoriesResponseModel> {
  //   return this.api.getStoriesData(payload).pipe(map((res: AppResponse<IStoriesResponseModel>) => res.data));
  // }
  //
  // getStory(payload: IGetStoryRequestModel): Observable<IStoryResponseDataModel> {
  //   return this.api.getStory(payload).pipe(map((res: AppResponse<IGetStoryResponseModel>) => res.data.story));
  // }
  //
  // deleteStory(payload: IDeleteStoryRequestModel): Observable<HttpResponseBody<any>> {
  //   return this.api.deleteStory(payload);
  // }
  //
  // createStory(payload: ICreateStoryRequestModelUnstringified) {
  //   const _payload = new FormData();
  //   _payload.append('story', JSON.stringify(payload.story));
  //   _payload.append('story_file', payload.story_file);
  //   if (payload.thumb) {
  //     _payload.append('thumb', payload.thumb);
  //   }
  //   return this.api.createStory(_payload);
  // }
  //
  // editStory(payload: ICreateStoryRequestModelUnstringified) {
  //   const _payload = new FormData();
  //   _payload.append('story', JSON.stringify(payload.story));
  //   if (payload.story_file) {
  //     _payload.append('story_file', payload.story_file);
  //   }
  //   return this.api.editStory(_payload);
  // }

}

import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {NotLazyService} from './not-lazy.service';
// import {IGetStoryRequestModel} from '../models/lazy.request.model';

@Injectable({
  providedIn: 'root'
})
export class NotLazyResolverService implements Resolve<Observable<any>> {

  constructor(private storiesService: NotLazyService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const StoryId = route.paramMap.get('name');
    // const payload: IGetStoryRequestModel = {
    //   story_id: StoryId
    // };
    //const stories = this.storiesService.getStory(payload);
    return of({});
  }
}

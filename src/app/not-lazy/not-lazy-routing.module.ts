import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {NotLazyOverviewComponent} from './containers/not-lazy-overview/not-lazy-overview.component';
import {NotLazyResolverService} from './services/not-lazy-resolver.service';
import {NotLazyEditComponent} from './containers/not-lazy-edit/not-lazy-edit.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'overview'},
  {
    path: 'overview',
    component: NotLazyOverviewComponent,
  },
  {
    path: 'edit/:name',
    component: NotLazyEditComponent,
    resolve: {
      storiesData: NotLazyResolverService
    }
  },
  {
    path: 'new',
    component: NotLazyEditComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotLazyRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotLazyOverviewComponent } from './containers/not-lazy-overview/not-lazy-overview.component';
import { NotLazyEditComponent } from './containers/not-lazy-edit/not-lazy-edit.component';

@NgModule({
  declarations: [NotLazyOverviewComponent, NotLazyEditComponent],
  imports: [
    CommonModule
  ]
})
export class NotLazyModule { }

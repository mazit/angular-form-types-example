import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {LazyOverviewComponent} from './containers/lazy-overview/lazy-overview.component';
import {LazyEditComponent} from './containers/lazy-edit/lazy-edit.component';
import {LazyResolverService} from './services/lazy-resolver.service';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'overview'},
  {
    path: 'overview',
    component: LazyOverviewComponent,
  },
  {
    path: 'edit/:name',
    component: LazyEditComponent,
    resolve: {
      storiesData: LazyResolverService
    }
  },
  {
    path: 'new',
    component: LazyEditComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LazyRoutingModule { }

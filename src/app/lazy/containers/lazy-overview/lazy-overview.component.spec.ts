import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LazyOverviewComponent } from './lazy-overview.component';

describe('LazyOverviewComponent', () => {
  let component: LazyOverviewComponent;
  let fixture: ComponentFixture<LazyOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LazyOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LazyOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

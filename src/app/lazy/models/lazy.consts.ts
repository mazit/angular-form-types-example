
export const NewStoryInitPayload = {
  story_id: '00000000-0000-0000-0000-000000000000',
  media_id: '00000000-0000-0000-0000-000000000000',
  gender: null,
  countries: [],
  start_date: new Date().toISOString(),
  end_date: new Date().toISOString(),
  likes_counter: 0,
  views_counter: 0,
  user_id: '00000000-0000-0000-0000-000000000000',
  media_height: 500,
  media_width: 500,
  duration: 0
}


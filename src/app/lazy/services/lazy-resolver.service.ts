import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {LazyService} from './lazy.service';

@Injectable({
  providedIn: 'root'
})
export class LazyResolverService implements Resolve<Observable<any>> {

  constructor(private storiesService: LazyService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const StoryId = route.paramMap.get('name');
    // const payload: IGetStoryRequestModel = {
    //   story_id: StoryId
    // };
   // const stories = this.storiesService.getStory(payload);
    return of({});
  }
}

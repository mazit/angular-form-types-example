import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LazyOverviewComponent } from './containers/lazy-overview/lazy-overview.component';
import { LazyEditComponent } from './containers/lazy-edit/lazy-edit.component';
import {LazyRoutingModule} from './lazy-routing.module';

@NgModule({
  declarations: [LazyOverviewComponent, LazyEditComponent],
  imports: [
    CommonModule,
    LazyRoutingModule
  ]
})
export class LazyModule { }

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TemplateFormComponent } from './template-form/components/template-form/template-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ReactiveFormComponent } from './reactive-form/components/reactive-form/reactive-form.component';
import {ThirdPartyFormModule} from './third-party-form/third-party-form.module';
import {AppRoutingModule} from './app-routing.module';
import {CommonModule} from '@angular/common';
import {NotLazyModule} from './not-lazy/not-lazy.module';


@NgModule({
  declarations: [
    AppComponent,
    TemplateFormComponent,
    ReactiveFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ThirdPartyFormModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ThirdPartyFormModule,
    NotLazyModule
  ],
  exports: [

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

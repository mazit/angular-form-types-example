import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NotLazyOverviewComponent} from './not-lazy/containers/not-lazy-overview/not-lazy-overview.component';
import {NotLazyModule} from './not-lazy/not-lazy.module';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'not-lazy/overview'},
  {
    path: 'lazy',
    loadChildren: './lazy/lazy.module#LazyModule',
  },
  {
    path: 'not-lazy/overview',
    component: NotLazyOverviewComponent,
  },
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
